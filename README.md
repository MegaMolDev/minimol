# README #

This is a very simple particle ray casting program.
Just compile what you have.

For Windows use the Visual Studio 2015 file located in the msvc subdirectory.
All dependencies should be automatically fetched with Nuget.

Infos on MMPLD data files can be found here:

http://megamol.org

https://svn.vis.uni-stuttgart.de/trac/megamol/wiki/mmpld