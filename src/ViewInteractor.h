#pragma once

#include <iostream>
#include <glm/glm.hpp>

namespace minimol {

// class to translate mouse and keyboard interactions to camera information
class ViewInteractor {

public:

    typedef glm::dvec3 vec;
    typedef double scalar;

    ViewInteractor();

    void updateModifiers(bool shift, bool alt, bool ctrl);
    void updateButtons(bool left, bool right, bool middle);
    void updateKeys(bool space);

    void updatePosition(int pos_x, int pos_y);
    void updateWheel(bool dir_up);

    void updateLeftButton(bool left);
    void updateRightButton(bool right);
    void updateMiddleButton(bool middle);

    void setViewportSize(int width, int height);

    vec eye, focus, up_dir;
    scalar zoom_speed;

    vec front_dir() const {
        return glm::normalize(focus - eye);
    }
    vec right_dir() const {
        return glm::normalize(glm::cross(up_dir, front_dir()));
    }

private:
    // flags that define the current state
    bool shift, alt, ctrl;
    bool left, right, middle;
    bool space;
    bool wheel_dir_up, wheel_active;
    bool position_valid;
    int pos_x, pos_y;
    int delta_x, delta_y;
    int old_pos_x, old_pos_y;
    int width, height;
    scalar sensitivity;

    void updateTransformation();
    void rotateSpherical(vec& target, const vec& center, vec& up, bool swap_y = false);
    void rotateUpDir(vec &target, const vec &axis);
    void moveImagePlane(vec& target, vec& center, const vec& up);
    void moveTowardsCenter(vec& target, vec& center, scalar step);
};

}
