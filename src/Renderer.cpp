#include "Renderer.h"
#include "Data.h"
#include <GL/glew.h>
#include <algorithm>
#include <glm/gtc/type_ptr.hpp>
#include <fstream>
#include <iostream>
#ifdef _WIN32
#include <Windows.h>
#endif


minimol::Renderer::glsl_shader::glsl_shader(unsigned int shader_type) : id(0) {
    id = ::glCreateShader(shader_type);
}

minimol::Renderer::glsl_shader::~glsl_shader(void) {
    ::glDeleteShader(id);
    id = 0;
}

minimol::Renderer::glsl_build_result minimol::Renderer::glsl_shader::compile_source(const char *src) {
    ::glShaderSource(id, 1, &src, nullptr);
    ::glCompileShader(id);
    GLint comp_stat = GL_FALSE;
    ::glGetShaderiv(id, GL_COMPILE_STATUS, &comp_stat);
    if (comp_stat == GL_FALSE) {
        glsl_build_result err(false);
        GLint log_len = 0;
        ::glGetShaderiv(id, GL_INFO_LOG_LENGTH, &log_len);
        std::vector<char> log(log_len);
        GLsizei log_len2;
        ::glGetShaderInfoLog(id, log_len, &log_len2, log.data());
        err.msg.assign(log.data(), log_len2);
        return err;
    }

    return true;
}


minimol::Renderer::glsl_program::glsl_program() : id(0), shaders() {
    id = ::glCreateProgram();
}

minimol::Renderer::glsl_program::~glsl_program() {
    if (id != 0) {
        ::glDeleteProgram(id);
        id = 0;
    }
    shaders.clear();
}

void minimol::Renderer::glsl_program::attach_shader(std::shared_ptr<glsl_shader> shader) {
    ::glAttachShader(id, shader->get_id());
    shaders.push_back(shader);
}

minimol::Renderer::glsl_build_result minimol::Renderer::glsl_program::link() {
    ::glLinkProgram(id);
    GLint link_stat = GL_FALSE;
    ::glGetProgramiv(id, GL_LINK_STATUS, &link_stat);
    if (link_stat == GL_FALSE) {
        glsl_build_result err(false);
        GLint log_len = 0;
        ::glGetProgramiv(id, GL_INFO_LOG_LENGTH, &log_len);
        std::vector<char> log(log_len);
        GLsizei log_len2;
        ::glGetProgramInfoLog(id, log_len, &log_len2, log.data());
        err.msg.assign(log.data(), log_len2);
        return err;
    }

    return true;
}


minimol::Renderer::Renderer(void) : dat(nullptr), inited(false), cam(nullptr), shader() {
}

minimol::Renderer::~Renderer(void) {
    this->dat = nullptr; // do not delete
}

void minimol::Renderer::init(void) {
    if (inited) {
        throw std::logic_error("Renderer must only be inited once");
    }

    std::shared_ptr<glsl_shader> vert, frag;
    vert.reset(new glsl_shader(GL_VERTEX_SHADER));
    frag.reset(new glsl_shader(GL_FRAGMENT_SHADER));

    try {
        if (vert->compile_source(this->loadShaderSource("sphere.glslvs").c_str()).succ == false) {
            throw std::runtime_error("return value false");
        }
    } catch (std::exception ex) {
        std::cerr << "Failed to compile vertex shader: " << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "Failed to compile vertex shader: unknown error" << std::endl;
    }
    try {
        if (frag->compile_source(this->loadShaderSource("sphere.glslfs").c_str()).succ == false) {
            throw std::runtime_error("return value false");
        }
    } catch (std::exception ex) {
        std::cerr << "Failed to compile fragment shader: " << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "Failed to compile fragment shader: unknown error" << std::endl;
    }

    shader.reset(new glsl_program());
    shader->attach_shader(vert);
    shader->attach_shader(frag);

    try {
        if (shader->link().succ == false) {
            throw std::runtime_error("return value false");
        }

        this->inited = true;
    } catch (std::exception ex) {
        std::cerr << "Failed to link shader program: " << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "Failed to link shader program: unknown error" << std::endl;
    }

}

void minimol::Renderer::deinit(void) {
    if (!this->inited) return;
    this->shader.reset();
    this->inited = false;
}

void minimol::Renderer::draw(void) const {
    if (this->dat == nullptr) return;

    ::glDisable(GL_LIGHTING);

    ::glUseProgram(this->shader->get_id());

    float viewportStuff[4];
    ::glGetFloatv(GL_VIEWPORT, viewportStuff);
    if (viewportStuff[2] < 1.0f) viewportStuff[2] = 1.0f;
    if (viewportStuff[3] < 1.0f) viewportStuff[3] = 1.0f;
    ::glPointSize(std::max<float>(viewportStuff[2], viewportStuff[3]));
    viewportStuff[2] = 2.0f / viewportStuff[2];
    viewportStuff[3] = 2.0f / viewportStuff[3];
    ::glUniform4fv(::glGetUniformLocation(this->shader->get_id(), "viewAttr"), 1, viewportStuff);
    ::glUniform3fv(::glGetUniformLocation(this->shader->get_id(), "camIn"), 1, glm::value_ptr(glm::vec3(this->cam->front_dir())));
    ::glUniform3fv(::glGetUniformLocation(this->shader->get_id(), "camRight"), 1, glm::value_ptr(glm::vec3(this->cam->right_dir())));
    ::glUniform3fv(::glGetUniformLocation(this->shader->get_id(), "camUp"), 1, glm::value_ptr(glm::vec3(this->cam->up_dir)));

    ::glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    GLuint colAryIdx = ::glGetAttribLocationARB(this->shader->get_id(), "colData");
    ::glColor3ub(255, 255, 255);

    for (size_t lidx = 0; lidx < this->dat->size(); lidx++) {
        const Data::List &list = (*this->dat)[static_cast<int>(lidx)];
        float minCol = 0.0f, maxCol = 1.0f;
        float globRad = -1.0f;

        // color data
        switch (list.GetColourDataType()) {
            case Data::COLDATA_NONE:
                ::glDisableVertexAttribArrayARB(colAryIdx);
                {
                    const unsigned char *gc = list.GetGlobalColour();
                    ::glVertexAttrib4fARB(colAryIdx, 
                        static_cast<float>(gc[0]) / 255.0f,
                        static_cast<float>(gc[1]) / 255.0f,
                        static_cast<float>(gc[2]) / 255.0f,
                        static_cast<float>(gc[3]) / 255.0f );
                }
                break;
            case Data::COLDATA_UINT8_RGB:
                ::glEnableVertexAttribArrayARB(colAryIdx);
                ::glVertexAttribPointerARB(colAryIdx, 3, GL_UNSIGNED_BYTE, true, list.GetColourDataStride(), list.GetColourData());
                break;
            case Data::COLDATA_UINT8_RGBA:
                ::glEnableVertexAttribArrayARB(colAryIdx);
                ::glVertexAttribPointerARB(colAryIdx, 4, GL_UNSIGNED_BYTE, true, list.GetColourDataStride(), list.GetColourData());
                break;
            case Data::COLDATA_FLOAT_RGB:
                ::glEnableVertexAttribArrayARB(colAryIdx);
                ::glVertexAttribPointerARB(colAryIdx, 3, GL_FLOAT, true, list.GetColourDataStride(), list.GetColourData());
                break;
            case Data::COLDATA_FLOAT_RGBA:
                ::glEnableVertexAttribArrayARB(colAryIdx);
                ::glVertexAttribPointerARB(colAryIdx, 4, GL_FLOAT, true, list.GetColourDataStride(), list.GetColourData());
                break;
            case Data::COLDATA_FLOAT_I: {
                ::glEnableVertexAttribArrayARB(colAryIdx);
                ::glVertexAttribPointerARB(colAryIdx, 1, GL_FLOAT, false, list.GetColourDataStride(), list.GetColourData());
                minCol = list.GetMinColourIndexValue();
                maxCol = list.GetMaxColourIndexValue();
            } break;
            default:
                ::glDisableVertexAttribArrayARB(colAryIdx);
                ::glVertexAttrib3fARB(colAryIdx, 0.5f, 0.5f, 0.5f);
                break;
        }

        // vertex data: position and radius
        switch (list.GetVertexDataType()) {
            case Data::VERTDATA_NONE:
                continue;
            case Data::VERTDATA_FLOAT_XYZ:
                glEnableClientState(GL_VERTEX_ARRAY);
                globRad = std::max<float>(0.00001f, list.GetGlobalRadius());
                glVertexPointer(3, GL_FLOAT, list.GetVertexDataStride(), list.GetVertexData());
                break;
            case Data::VERTDATA_FLOAT_XYZR:
                glEnableClientState(GL_VERTEX_ARRAY);
                glVertexPointer(4, GL_FLOAT, list.GetVertexDataStride(), list.GetVertexData());
                break;
            default:
                continue;
        }

        ::glUniform3f(::glGetUniformLocation(this->shader->get_id(), "globParams"), globRad, minCol, 1.0f / (maxCol - minCol));

        ::glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(list.GetCount()));
    }

    ::glUseProgram(0);

    ::glDisableVertexAttribArrayARB(colAryIdx);
    ::glDisableClientState(GL_VERTEX_ARRAY);
    ::glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);

}

std::string minimol::Renderer::loadShaderSource(const char* filename) {
    const int max_dir_ascent = 6;
    std::string path;
    FILE *f(nullptr);

    // first search based on application directory
    std::string extDir;
#ifdef WIN32
    char extDirBuf[MAX_PATH];
    int strLen = GetModuleFileName(nullptr, extDirBuf, MAX_PATH);
    if (strLen != 0) {
        extDir = extDirBuf;
        std::replace_if(extDir.begin(), extDir.end(), [](const char& c) { return c == '\\'; }, '/');
        std::string s("/");
        auto e = std::find_end(extDir.begin(), extDir.end(), s.begin(), s.end());
        if (e != extDir.end()) {
            extDir.resize(std::distance(extDir.begin(), e) + 1, 0);
        }
    } else {
        extDir = "";
    }
#else
    // We could use something like: http://stackoverflow.com/questions/933850/how-to-find-the-location-of-the-executable-in-c
    extDir = "";
#endif /* WIN32 */

    path = extDir;
    path += filename;
    for (int i = 0; (i < max_dir_ascent) && (f == nullptr); i++) {
        f = fopen(path.c_str(), "rt");
        if (f != nullptr) break;

        path = extDir;
        path += "src/";
        path += filename;

        f = fopen(path.c_str(), "rt");
        if (f != nullptr) break;

        extDir += "../";
        path = extDir;
        path += filename;
    }

    // second search on execution directory
    if (f == nullptr) {
        extDir = "";
        path = extDir;
        path += filename;
        for (int i = 0; (i < max_dir_ascent) && (f == nullptr); i++) {
            f = fopen(path.c_str(), "rt");
            if (f != nullptr) break;

            path = extDir;
            path += "src/";
            path += filename;

            f = fopen(path.c_str(), "rt");
            if (f != nullptr) break;

            extDir += "../";
            path = extDir;
            path += filename;
        }
    }

    if (f == nullptr) {
        std::string msg("Unable to open shader file ");
        msg += filename;
        throw std::runtime_error(msg);
    }

    fclose(f);

    std::ifstream file(path.c_str());
    std::string str;

    file.seekg(0, std::ios::end);
    str.reserve(static_cast<std::string::size_type>(file.tellg()));
    file.seekg(0, std::ios::beg);

    str.assign((std::istreambuf_iterator<char>(file)),
                std::istreambuf_iterator<char>());

    return str;
}
