#include "MMPLDWriter.h"
#include <iostream>
#include <cstdint>

namespace minimol {

MMPLDWriter::MMPLDWriter() : file(), frame_i(0), frame_cnt(0), version(Version_1_0) {
    // intentionally empty
}

MMPLDWriter::~MMPLDWriter() {
    CloseFile();
}

bool MMPLDWriter::CreateOpenFile(const std::string& path, unsigned int frameCnt, FileFormatVersion version) {
    if (frameCnt == 0) {
        std::cerr << "'frameCnt' must not be zero" << std::endl;
        return false;
    }
    if (file.is_open()) {
        std::cerr << "MMPLDFile already opened" << std::endl;
        return false;
    }

    file.open(path, std::ios::binary | std::ios::trunc);
    if (!file.is_open()) {
        std::cerr << "Cannot open MMPLDFile" << std::endl;
        return false;
    }

    this->version = version;

    frame_i = 0;
    frame_cnt = frameCnt;

    file.write("MMPLD\0", 6);
    uint16_t ver = 0;
    file.write(reinterpret_cast<const char*>(&ver), 2); // value will be fixed on close
    uint32_t frame_c = frame_cnt;
    file.write(reinterpret_cast<const char*>(&frame_c), 4);
    float bbox[6];
    file.write(reinterpret_cast<const char*>(&bbox), 6 * 4); // value will be fixed by first WriteDataFrame
    file.write(reinterpret_cast<const char*>(&bbox), 6 * 4); // value will be fixed by first WriteDataFrame
    uint64_t seekPos = 60 + (1 + frame_cnt) * 8;
    for (unsigned int i = 0; i <= frame_cnt; ++i) {
        file.write(reinterpret_cast<const char*>(&seekPos), 8); // value will be fixed by WriteDataFrame
        seekPos = 0;
    }

    assert(static_cast<uint64_t>(file.tellp()) == (60 + (1 + frame_cnt) * 8));

    return true;
}

bool MMPLDWriter::WriteDataFrame(const Data& data) {
    if (!file.is_open()) {
        std::cerr << "MMPLDFile not open" << std::endl;
        return false;
    }
    if (frame_i >= frame_cnt) {
        std::cerr << "All data frames have already been written" << std::endl;
        return false;
    }

    if (frame_i == 0) {
        // write bounding box infos
        float bbox[6];

        std::fstream::pos_type l = file.tellp();
        file.seekp(12);

        bbox[0] = data.BBox()[0].x;
        bbox[1] = data.BBox()[0].y;
        bbox[2] = data.BBox()[0].z;
        bbox[3] = data.BBox()[1].x;
        bbox[4] = data.BBox()[1].y;
        bbox[5] = data.BBox()[1].z;
        file.write(reinterpret_cast<const char*>(&bbox), 6 * 4);

        bbox[0] = data.CBox()[0].x;
        bbox[1] = data.CBox()[0].y;
        bbox[2] = data.CBox()[0].z;
        bbox[3] = data.CBox()[1].x;
        bbox[4] = data.CBox()[1].y;
        bbox[5] = data.CBox()[1].z;
        file.write(reinterpret_cast<const char*>(&bbox), 6 * 4);

        file.seekp(l);
    }

    if (version == Version_1_2) {
        float ts = data.get_timestamp();
        file.write(reinterpret_cast<const char*>(&ts), 4);
    }

    uint32_t list_cnt = static_cast<uint32_t>(data.size());
    file.write(reinterpret_cast<const char*>(&list_cnt), 4);

    for (uint32_t list_i = 0; list_i < list_cnt; ++list_i) {
        const Data::List& list = data[static_cast<int>(list_i)];

        uint8_t vt = 0, ct = 0;
        unsigned int vs = 0, vo = 0, cs = 0, co = 0;
        switch(list.GetVertexDataType()) {
            case Data::VERTDATA_NONE: vt = 0; vs = 0; break;
            case Data::VERTDATA_FLOAT_XYZ: vt = 1; vs = 12; break;
            case Data::VERTDATA_FLOAT_XYZR: vt = 2; vs = 16; break;
            case Data::VERTDATA_SHORT_XYZ: vt = 3; vs = 6; break;
            default: vt = 0; vs = 0; break;
        }
        if (vt != 0) {
            switch(list.GetColourDataType()) {
                case Data::COLDATA_NONE: ct = 0; cs = 0; break;
                case Data::COLDATA_UINT8_RGB: ct = 1; cs = 3; break;
                case Data::COLDATA_UINT8_RGBA: ct = 2; cs = 4; break;
                case Data::COLDATA_FLOAT_I: ct = 3; cs = 4; break;
                case Data::COLDATA_FLOAT_RGB: ct = 4; cs = 12; break;
                case Data::COLDATA_FLOAT_RGBA: ct = 5; cs = 16; break;
                default: ct = 0; cs = 0; break;
            }
        } else {
            ct = 0;
        }
        file.write(reinterpret_cast<const char*>(&vt), 1);
        file.write(reinterpret_cast<const char*>(&ct), 1);

        if (list.GetVertexDataStride() > vs) {
            vo = list.GetVertexDataStride();
        } else {
            vo = vs;
        }
        if (list.GetColourDataStride() > cs) {
            co = list.GetColourDataStride();
        } else {
            co = cs;
        }

        if ((vt == 1) || (vt == 3)) {
            float f = list.GetGlobalRadius();
            file.write(reinterpret_cast<const char*>(&f), 4);
        }
        if (ct == 0) {
            const unsigned char *col = list.GetGlobalColour();
            file.write(reinterpret_cast<const char*>(&col), 4);
        } else if (ct == 3) {
            float f = list.GetMinColourIndexValue();
            file.write(reinterpret_cast<const char*>(&f), 4);
            f = list.GetMaxColourIndexValue();
            file.write(reinterpret_cast<const char*>(&f), 4);
        }

        uint64_t cnt = list.GetCount();
        if (vt == 0) cnt = 0;
        file.write(reinterpret_cast<const char*>(&cnt), 8);
        if (vt == 0) continue;
        const unsigned char *vp = static_cast<const unsigned char *>(list.GetVertexData());
        const unsigned char *cp = static_cast<const unsigned char *>(list.GetColourData());
        for (uint64_t i = 0; i < cnt; i++) {
            file.write(reinterpret_cast<const char*>(vp), vs);
            vp += vo;
            if (ct != 0) {
                file.write(reinterpret_cast<const char*>(cp), cs);
                cp += co;
            }
        }

    }

    frame_i++;
    std::fstream::pos_type frame_end = file.tellp();
    file.seekp(60 + (frame_i * 8));
    uint64_t frame_end_seek = static_cast<uint64_t>(frame_end);
    file.write(reinterpret_cast<const char*>(&frame_end_seek), 8);
    file.seekp(frame_end);

    return true;
}

bool MMPLDWriter::CloseFile() {
    if (!file.is_open()) return true; // fail silently
    if (frame_i < frame_cnt) {
        std::cerr << "Too few data frames written" << std::endl;
        return false;
    }

    file.seekp(6);
    uint16_t version = 100;
    if (this->version == Version_1_2) version = 102;
    file.write(reinterpret_cast<const char*>(&version), 2);
    file.seekp(0, std::ios::end);

    file.close();

    return true;
}

}
