#pragma once

#ifdef HAS_LIBPNG

namespace minimol {

    class ImageWriter {
    public:

        enum class DataType {
            RGBu8,
            RGBAu8,
            Df
        };

        ImageWriter(void);
        ~ImageWriter(void);

        // If DataType is Df the content data is pointing to will be changed!
        void save(const char* fn, unsigned int width, unsigned int height, DataType type, void* data);
    };

}

#endif
