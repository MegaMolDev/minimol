#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <memory>
#include <string>
#include "ViewInteractor.h"

namespace minimol {

    class Data;

    class Renderer {
    public:
        Renderer(void);
        ~Renderer(void);
        inline void SetData(Data *data) {
            this->dat = data;
        }
        inline void SetCamParam(const ViewInteractor *cam) {
            this->cam = cam;
        }

        void init(void);
        void deinit(void);

        void draw(void) const;

    private:

        struct glsl_build_result {
            glsl_build_result() = delete;
            glsl_build_result(bool b) : succ(b), msg() {}
            std::string msg;
            bool succ;
        };

        class glsl_shader {
        public:
            glsl_shader(unsigned int shader_type);
            glsl_shader(const glsl_shader& src) = delete;
            ~glsl_shader();
            inline unsigned int get_id() const { return id; }
            glsl_build_result compile_source(const char *src);
            glsl_shader& operator=(const glsl_shader& src) = delete;
        private:
            unsigned int id;
        };

        class glsl_program {
        public:
            glsl_program();
            glsl_program(const glsl_program& src) = delete;
            ~glsl_program();

            inline unsigned int get_id() const { return id; }

            void attach_shader(std::shared_ptr<glsl_shader> shader);

            glsl_build_result link();

            glsl_program& operator=(const glsl_program& src) = delete;
        private:

            unsigned int id;
            std::vector<std::shared_ptr<glsl_shader> > shaders;
        };

        std::string loadShaderSource(const char* filename);

        Data *dat;
        bool inited;
        const ViewInteractor *cam;
        std::shared_ptr<glsl_program> shader;
    };

}
