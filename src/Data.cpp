#include "Data.h"
#include <stdexcept>
#include <fstream>

namespace minimol {

Data::Data(void) : filename(), filetime(0), bbox(), clipbox(), data(), lists() {
    this->bbox[0] = glm::vec3(0.0f);
    this->bbox[1] = glm::vec3(1.0f);
    this->clipbox[0] = glm::vec3(0.0f);
    this->clipbox[1] = glm::vec3(1.0f);
}

Data::~Data(void) {
    this->lists.clear();
    this->data.clear();
}

void Data::loadMMPLD(const char* filename, unsigned int time) {
    try {
        // clear
        this->bbox[0] = glm::vec3(0.0f);
        this->bbox[1] = glm::vec3(1.0f);
        this->clipbox[0] = glm::vec3(0.0f);
        this->clipbox[1] = glm::vec3(1.0f);
        this->lists.clear();
        this->data.clear();

        // load
        std::ifstream file(filename, std::ios::binary);
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        if (!file.is_open()) throw std::logic_error("Failed to open data file");

        char magicid[6];
        file.read((char*)magicid, 6);
        if (::memcmp(magicid, "MMPLD", 6) != 0) {
            throw std::runtime_error("MMPLD file header id wrong");
        }
        uint16_t ver;
        file.read((char*)&ver, 2);
        if (ver != 100 && ver != 101 && ver != 102) {
            throw std::runtime_error("MMPLD file header version wrong");
        }
        uint32_t frmCnt = 0;
        file.read((char*)&frmCnt, 4);
        if (frmCnt == 0) {
            throw std::runtime_error("MMPLD file does not contain any frame information");
        }
        if (frmCnt <= time) {
            time = frmCnt - 1;
            fprintf(stderr, "Requested time not found in file. Time clamped to %u\n", time);
        }

        float box[6];
        file.read((char*)box, 4 * 6);
        this->bbox[0] = ::glm::vec3(box[0], box[1], box[2]);
        this->bbox[1] = ::glm::vec3(box[3], box[4], box[5]);
        file.read((char*)box, 4 * 6);
        this->clipbox[0] = ::glm::vec3(box[0], box[1], box[2]);
        this->clipbox[1] = ::glm::vec3(box[3], box[4], box[5]);

        std::vector<uint64_t> frameIdx(frmCnt + 1);
        file.read((char*)frameIdx.data(), 8 * (frmCnt + 1));

        file.rdbuf()->pubseekpos(frameIdx[time]);

        this->data.resize(static_cast<std::vector<uint8_t>::size_type>(frameIdx[time + 1] - frameIdx[time]));

        file.read((char*)this->data.data(), this->data.size());
        file.close();

        size_t p = 0;
        if ((ver == 100) || (ver == 101)) {
            timestamp = static_cast<float>(time);
        } else if (ver == 102) {
            timestamp = *reinterpret_cast<float*>(this->data.data() + p);
            p += sizeof(float);
            static_assert(sizeof(float) == 4, "float size wrong");
        } else {
            fprintf(stderr, "Internal data read error %d", __LINE__);
        }
        uint32_t cnt = *reinterpret_cast<uint32_t*>(this->data.data() + p);
        p += sizeof(uint32_t);
        static_assert(sizeof(uint32_t) == 4, "uint32_t size wrong");
        this->lists.resize(cnt);
        for (uint32_t i = 0; i < cnt; i++) {
            List &pts = this->lists[i];

            uint8_t vrtType = this->data.at(p); p += 1;
            uint8_t colType = this->data.at(p); p += 1;
            VertexDataType vrtDatType;
            ColourDataType colDatType;
            size_t vrtSize = 0;
            size_t colSize = 0;

            switch (vrtType) {
                case 0: vrtSize = 0; vrtDatType = VERTDATA_NONE; break;
                case 1: vrtSize = 12; vrtDatType = VERTDATA_FLOAT_XYZ; break;
                case 2: vrtSize = 16; vrtDatType = VERTDATA_FLOAT_XYZR; break;
                case 3: vrtSize = 6; vrtDatType = VERTDATA_SHORT_XYZ; break;
                default: vrtSize = 0; vrtDatType = VERTDATA_NONE; break;
            }
            if (vrtType != 0) {
                switch (colType) {
                    case 0: colSize = 0; colDatType = COLDATA_NONE; break;
                    case 1: colSize = 3; colDatType = COLDATA_UINT8_RGB; break;
                    case 2: colSize = 4; colDatType = COLDATA_UINT8_RGBA; break;
                    case 3: colSize = 4; colDatType = COLDATA_FLOAT_I; break;
                    case 4: colSize = 12; colDatType = COLDATA_FLOAT_RGB; break;
                    case 5: colSize = 16; colDatType = COLDATA_FLOAT_RGBA; break;
                    default: colSize = 0; colDatType = COLDATA_NONE; break;
                }
            } else {
                colDatType = COLDATA_NONE;
                colSize = 0;
            }
            unsigned int stride = static_cast<unsigned int>(vrtSize + colSize);

            if ((vrtType == 1) || (vrtType == 3)) {
                pts.SetGlobalRadius(*reinterpret_cast<float*>(this->data.data() + p)); p += 4;
            } else {
                pts.SetGlobalRadius(0.05f);
            }

            if (colType == 0) {
                pts.SetGlobalColour(this->data.at(p),
                    this->data.at(p + 1),
                    this->data.at(p + 2));
                p += 4;
            } else {
                pts.SetGlobalColour(192, 192, 192);
                if (colType == 3) {
                    pts.SetColourMapIndexValues(
                        *reinterpret_cast<float*>(this->data.data() + p),
                        *reinterpret_cast<float*>(this->data.data() + p + 4));
                    p += 8;
                } else {
                    pts.SetColourMapIndexValues(0.0f, 1.0f);
                }
            }

            pts.SetCount(*reinterpret_cast<uint64_t*>(this->data.data() + p)); p += 8;

            pts.SetVertexData(vrtDatType, this->data.data() + p, stride);
            pts.SetColourData(colDatType, this->data.data() + p + vrtSize, stride);

            p += static_cast<size_t>(stride * pts.GetCount());
        }

        this->filename = filename;
        this->filetime = time;
    } catch(...) {
        throw;
    }
}

size_t Data::size() const {
    return this->lists.size();
}

const Data::List& Data::operator[](int idx) const {
    return this->lists[idx];
}

}
