#pragma once

#include <string>
#include "Data.h"
#include <fstream>

namespace minimol {

class MMPLDWriter {
public:
    enum FileFormatVersion {
        Version_1_0,
        Version_1_2
    };

    MMPLDWriter();
    MMPLDWriter(const MMPLDWriter& src) = delete;
    ~MMPLDWriter();
    MMPLDWriter& operator=(const MMPLDWriter& rhs) = delete;

    /**
     * Creates and opens a file for writing. If the file exists, it will be overwritten without warning.
     *
     * @param path The path to the file to be created
     * @param frameCnt The number of data frames to be written
     *
     * @return True on success
     *
     * @remarks frameCnt must be the correct value.
     *    After the file has been successfully created, 'WriteDataFrame' must called exactly that often.
     *    The 'CloseFile' has to be called.
     */
    bool CreateOpenFile(const std::string& path, unsigned int frameCnt, FileFormatVersion version = Version_1_0);
    bool WriteDataFrame(const Data& data);
    bool CloseFile();

private:
    std::ofstream file;
    unsigned int frame_i;
    unsigned int frame_cnt;
    FileFormatVersion version;
};

}
