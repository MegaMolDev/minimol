#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "Window.h"
#include "Data.h"
#include "Renderer.h"
#include "CmdLineParser.h"


int main(int argc, char* argv[]) {
    std::cout << std::endl
        << "MiniMol reference implementation" << std::endl
        << "Copyright 2014-2015 (C) by S. Grottel, TU Dresden" << std::endl
        << std::endl;

    try {

        ::glutInit(&argc, argv);

        minimol::CmdLineParser parser;
        if (!parser.Parse(argc, argv, false)) {
            return 0;
        }

        ::glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
        ::glutInitWindowSize(1280, 720);
        ::glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
        ::glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

        minimol::Window wnd;
        minimol::Data dat;
        minimol::Renderer rnd;
        wnd.SetData(&dat);
        rnd.SetData(&dat);
        wnd.SetRenderer(&rnd);

        std::string fileName;
        unsigned int fileTime;
        if (parser.DataFile(fileName, fileTime)) {
            dat.loadMMPLD(fileName.c_str(), fileTime);
            wnd.SetData(&dat); // reset cam as sfx
        } else {
            glm::vec3 boxMin, boxMax;
            if (parser.BBox(boxMin, boxMax)) dat.ForceBBox(boxMin, boxMax);
            if (parser.CBox(boxMin, boxMax)) dat.ForceCBox(boxMin, boxMax);
        }

        wnd.SetFromParser(parser);

        ::glutMainLoop();

        return 0;

    } catch(std::exception ex) {
        std::cerr << "Terminated with std::exception: " << ex.what() << std::endl;
    } catch(...) {
        std::cerr << "Terminated with unknown exception" << std::endl;
    }
    return -1;
}
