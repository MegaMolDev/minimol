#pragma once

#include <string>
#include <unordered_map>
#include <vector>

namespace minimol {

    class StateFile {
    public:
        StateFile(void);
        ~StateFile(void);

        inline void clear() {
            this->values.clear();
        }

        inline std::string& operator[](const std::string& key) {
            return this->values[key];
        }
        inline const std::string& operator[](const std::string& key) const {
            return this->values.at(key);
        }
        inline bool has_key(const std::string& key) {
            return this->values.find(key) != this->values.end();
        }
        
        std::vector<std::string> keys(void) const;
        
        template<typename T>
        inline bool try_assign(const std::string& key, T setter) {
            auto it = this->values.find(key);
            if (it != this->values.end()) {
                try {
                    setter(it->second);
                    return true;
                } catch(...) {
                }
            }
            return false;
        }
        template<typename T>
        inline bool try_assign_int(const std::string& key, T setter) {
            auto it = this->values.find(key);
            if (it != this->values.end()) {
                try {
                    setter(StateFile::str_to_int(it->second));
                    return true;
                } catch(...) {
                }
            }
            return false;
        }
        template<typename T>
        inline bool try_assign_double(const std::string& key, T setter) {
            auto it = this->values.find(key);
            if (it != this->values.end()) {
                try {
                    setter(StateFile::str_to_double(it->second));
                    return true;
                } catch(...) {
                }
            }
            return false;
        }

        void save(std::string path);
        void load(std::string path);

        static inline int str_to_int(const std::string& str) {
            return std::stoi(str);
        }
        static inline std::string int_to_str(int i) {
            return std::to_string(i);
        }

        static inline double str_to_double(const std::string& str) {
            return std::stod(str);
        }
        static inline std::string double_to_str(double i) {
            return std::to_string(i);
        }

    private:

        std::unordered_map<std::string, std::string> values;
    };

}
