#include "CmdLineParser.h"

namespace minimol {

CmdLineParser::CmdLineParser(void) : datafile(), winSize(), cam(), bbox(), cbox(), showBBox(), light(), isCamLight() {
    // intentionally empty
}

CmdLineParser::~CmdLineParser(void) {
    // intentionally empty
}

bool CmdLineParser::Parse(int argc, char **argv, bool includeFirstArgument) {

    for (int argi = includeFirstArgument ? 0 : 1; argi < argc; ++argi) {
        char *arg = argv[argi];

        if ((std::string("-h").compare(arg) == 0) || (std::string("--help").compare(arg) == 0)) {
            std::cout << "Syntax:" << std::endl
                << "  -h --help   Prints this help message" << std::endl
                << "  -d --data   Specifies <file path> and <time frame> to load" << std::endl
                << "  --win       Specifies <width> and <height> for the window" << std::endl
                << "  --cam       Specifies Camera parameters: vec3(pos), vec3(lookAt), vec3(up), float(apAng)" << std::endl
                << "  --bbox      Specifies data set bounding box: vec3(min), vec3(max)" << std::endl
                << "  --cbox      Specifies data set clipping box: vec3(min), vec3(max)" << std::endl
                << "  --drawBBox  If set, draws the bounding box" << std::endl
                << "  --light     Specifies the light parameters: vec3(dir)" << std::endl
                << "  --camLight  If set, the light position is relative to the camera" << std::endl
                << std::endl;
            std::cout << "Example:" << std::endl
                << "  minimol.exe -d dataset.mmpld 0" << std::endl;
            return false;
        } else 
        if ((std::string("-d").compare(arg) == 0) || (std::string("--data").compare(arg) == 0)) {
            if (argi + 2 >= argc) {
                std::cerr << "Command line truncated: --data expects 2 arguments" << std::endl;
                return false;
            }
            datafile.reset(new std::tuple<std::string, unsigned int>(argv[argi + 1], std::atoi(argv[argi + 2])));
            argi += 2;

        } else 
        if (std::string("--win").compare(arg) == 0) {
            if (argi + 2 >= argc) {
                std::cerr << "Command line truncated: --win expects 2 arguments" << std::endl;
                return false;
            }
            winSize.reset(new std::tuple<unsigned int, unsigned int>(std::atoi(argv[argi + 1]), std::atoi(argv[argi + 2])));
            argi += 2;

        } else 
        if (std::string("--cam").compare(arg) == 0) {
            if (argi + 10 >= argc) {
                std::cerr << "Command line truncated: --cam expects 10 arguments" << std::endl;
                return false;
            }
            cam.reset(new std::tuple<glm::vec3, glm::vec3, glm::vec3, float>(
                glm::vec3(std::atof(argv[argi + 1]), std::atof(argv[argi + 2]), std::atof(argv[argi + 3])),
                glm::vec3(std::atof(argv[argi + 4]), std::atof(argv[argi + 5]), std::atof(argv[argi + 6])),
                glm::vec3(std::atof(argv[argi + 7]), std::atof(argv[argi + 8]), std::atof(argv[argi + 9])),
                static_cast<float>(std::atof(argv[argi + 10]))
            ));
            argi += 10;

        } else 
        if (std::string("--bbox").compare(arg) == 0) {
            if (argi + 6 >= argc) {
                std::cerr << "Command line truncated: --bbox expects 6 arguments" << std::endl;
                return false;
            }
            bbox.reset(new std::tuple<glm::vec3, glm::vec3>(
                glm::vec3(std::atof(argv[argi + 1]), std::atof(argv[argi + 2]), std::atof(argv[argi + 3])),
                glm::vec3(std::atof(argv[argi + 4]), std::atof(argv[argi + 5]), std::atof(argv[argi + 6]))
            ));
            argi += 6;

        } else 
        if (std::string("--cbox").compare(arg) == 0) {
            if (argi + 6 >= argc) {
                std::cerr << "Command line truncated: --cbox expects 6 arguments" << std::endl;
                return false;
            }
            cbox.reset(new std::tuple<glm::vec3, glm::vec3>(
                glm::vec3(std::atof(argv[argi + 1]), std::atof(argv[argi + 2]), std::atof(argv[argi + 3])),
                glm::vec3(std::atof(argv[argi + 4]), std::atof(argv[argi + 5]), std::atof(argv[argi + 6]))
            ));
            argi += 6;

        } else 
        if (std::string("--drawBBox").compare(arg) == 0) {
            showBBox.reset(new bool(true));

        } else 
        if (std::string("--light").compare(arg) == 0) {
            if (argi + 3 >= argc) {
                std::cerr << "Command line truncated: --light expects 3 arguments" << std::endl;
                return false;
            }
            light.reset(new glm::vec3(std::atof(argv[argi + 1]), std::atof(argv[argi + 2]), std::atof(argv[argi + 3])));
            argi += 3;

        } else 
        if (std::string("--camLight").compare(arg) == 0) {
            isCamLight.reset(new bool(true));

        } else {
            std::cerr << "Unexpected command line argument " << argi << std::endl;
            return false;
        }
    }

    return true;
}

}
