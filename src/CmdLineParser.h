#pragma once

#include <iostream>
#include <algorithm>
#include <glm/glm.hpp>
#include <string>
#include <memory>
#include <tuple>

namespace minimol {

    class CmdLineParser {
    public:

        CmdLineParser(void);
        ~CmdLineParser(void);
        bool Parse(int argc, char **argv, bool includeFirstArgument = true);

        bool DataFile(std::string& outPath, unsigned int& outTime) const {
            if (!datafile) return false;
            outPath = std::get<0>(*datafile);
            outTime = std::get<1>(*datafile);
            return true;
        }

        bool WindowSize(unsigned int& outWidth, unsigned int& outHeight) const {
            if (!winSize) return false;
            outWidth = std::get<0>(*winSize);
            outHeight = std::get<1>(*winSize);
            return true;
        }

        bool Camera(glm::vec3& outPos, glm::vec3& outLookAt, glm::vec3& outUp, float& outApAng) const {
            if (!cam) return false;
            outPos = std::get<0>(*cam);
            outLookAt = std::get<1>(*cam);
            outUp = std::get<2>(*cam);
            outApAng = std::get<3>(*cam);
            return true;
        }

        bool BBox(glm::vec3& outBBoxMin, glm::vec3& outBBoxMax) const {
            if (!bbox) return false;
            outBBoxMin = std::get<0>(*bbox);
            outBBoxMax = std::get<1>(*bbox);
            return true;
        }

        bool CBox(glm::vec3& outCBoxMin, glm::vec3& outCBoxMax) const {
            if (!cbox) return false;
            outCBoxMin = std::get<0>(*cbox);
            outCBoxMax = std::get<1>(*cbox);
            return true;
        }

        bool ShowBBox(bool& outShowBox) const {
            if (!showBBox) return false;
            outShowBox = *showBBox;
            return true;
        }

        bool Light(glm::vec3& outLight) const {
            if (!light) return false;
            outLight = *light;
            return true;
        }

        bool CamLight(bool& outIsCamLight) const {
            if (!isCamLight) return false;
            outIsCamLight = *isCamLight;
            return true;
        }

    private:

        std::shared_ptr<std::tuple<std::string, unsigned int> > datafile;
        std::shared_ptr<std::tuple<unsigned int, unsigned int> > winSize;
        std::shared_ptr<std::tuple<glm::vec3, glm::vec3, glm::vec3, float> > cam;
        std::shared_ptr<std::tuple<glm::vec3, glm::vec3> > bbox;
        std::shared_ptr<std::tuple<glm::vec3, glm::vec3> > cbox;
        std::shared_ptr<bool> showBBox;
        std::shared_ptr<glm::vec3> light;
        std::shared_ptr<bool> isCamLight;

    };

}
