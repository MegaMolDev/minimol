#pragma once

#include <cstdint>
#include <glm/glm.hpp>
#include <vector>
#include <string>

namespace minimol {

class Data {
public:

    /* Copied from MegaMol MultiParticleDataCall */
    /** possible values for the vertex data */
    enum VertexDataType {
        VERTDATA_NONE, //< indicates that this object is void
        VERTDATA_FLOAT_XYZ, //< use global radius
        VERTDATA_FLOAT_XYZR,
        VERTDATA_SHORT_XYZ //< quantized positions and global radius
    };

    /** possible values for the colour data */
    enum ColourDataType {
        COLDATA_NONE, //< use global colour
        COLDATA_UINT8_RGB,
        COLDATA_UINT8_RGBA,
        COLDATA_FLOAT_RGB,
        COLDATA_FLOAT_RGBA,
        COLDATA_FLOAT_I //< single float value to be mapped by a transfer function
    };

    class List {
    public:
        List() : colDataType(COLDATA_NONE), colPtr(nullptr), colStride(0),
                count(0), maxColI(1.0f), minColI(0.0f), radius(0.5f),
                vertDataType(VERTDATA_NONE), vertPtr(nullptr), vertStride(0) {
            this->col[0] = 255;
            this->col[1] = 0;
            this->col[2] = 0;
            this->col[3] = 255;
        }
        List(const List& src) {
            *this = src;
        }
        ~List() {
            this->SetCount(0); // clears ptrs as sfx
        }
        inline ColourDataType GetColourDataType(void) const {
            return this->colDataType;
        }
        inline const void * GetColourData(void) const {
            return this->colPtr;
        }
        inline unsigned int GetColourDataStride(void) const {
            return this->colStride;
        }
        inline uint64_t GetCount(void) const {
            return this->count;
        }
        inline const uint8_t * GetGlobalColour(void) const {
            return this->col;
        }
        inline float GetGlobalRadius(void) const {
            return this->radius;
        }
        inline float GetMaxColourIndexValue(void) const {
            return this->maxColI;
        }
        inline float GetMinColourIndexValue(void) const {
            return this->minColI;
        }
        inline VertexDataType GetVertexDataType(void) const {
            return this->vertDataType;
        }
        inline const void * GetVertexData(void) const {
            return this->vertPtr;
        }
        inline unsigned int GetVertexDataStride(void) const {
            return this->vertStride;
        }

        void SetColourData(ColourDataType t, const void *p, unsigned int s = 0) {
            this->colDataType = t;
            this->colPtr = p;
            this->colStride = s;
        }
        void SetColourMapIndexValues(float minVal, float maxVal) {
            this->maxColI = maxVal;
            this->minColI = minVal;
        }
        void SetCount(uint64_t cnt) {
            this->colDataType = COLDATA_NONE;
            this->colPtr = nullptr; // DO NOT DELETE
            this->vertDataType = VERTDATA_NONE;
            this->vertPtr = nullptr; // DO NOT DELETE
            this->count = cnt;
        }
        void SetGlobalColour(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255u) {
            this->col[0] = r;
            this->col[1] = g;
            this->col[2] = b;
            this->col[3] = a;
        }
        void SetGlobalRadius(float r) {
            this->radius = r;
        }
        void SetVertexData(VertexDataType t, const void *p,
                unsigned int s = 0) {
            //ASSERT(this->disabledNullChecks || (p != NULL) || (t == VERTDATA_NONE));
            this->vertDataType = t;
            this->vertPtr = p;
            this->vertStride = s;
        }

        List& operator=(const List& rhs) {
            this->col[0] = rhs.col[0];
            this->col[1] = rhs.col[1];
            this->col[2] = rhs.col[2];
            this->col[3] = rhs.col[3];
            this->colDataType = rhs.colDataType;
            this->colPtr = rhs.colPtr;
            this->colStride = rhs.colStride;
            this->count = rhs.count;
            this->maxColI = rhs.maxColI;
            this->minColI = rhs.minColI;
            this->radius = rhs.radius;
            this->vertDataType = rhs.vertDataType;
            this->vertPtr = rhs.vertPtr;
            this->vertStride = rhs.vertStride;
            return *this;
        }
        bool operator==(const List& rhs) const {
            return ((this->col[0] == rhs.col[0])
                && (this->col[1] == rhs.col[1])
                && (this->col[2] == rhs.col[2])
                && (this->col[3] == rhs.col[3])
                && (this->colDataType == rhs.colDataType)
                && (this->colPtr == rhs.colPtr)
                && (this->colStride == rhs.colStride)
                && (this->count == rhs.count)
                && (this->maxColI == rhs.maxColI)
                && (this->minColI == rhs.minColI)
                && (this->radius == rhs.radius)
                && (this->vertDataType == rhs.vertDataType)
                && (this->vertPtr == rhs.vertPtr)
                && (this->vertStride == rhs.vertStride));
        }

    private:

        uint8_t col[4];
        ColourDataType colDataType;
        const void *colPtr;
        unsigned int colStride;
        uint64_t count;
        float maxColI;
        float minColI;
        float radius;
        VertexDataType vertDataType;
        const void *vertPtr;
        unsigned int vertStride;

    };

    Data(void);
    ~Data(void);

    void loadMMPLD(const char* filename, unsigned int time = 0);

    inline const glm::vec3 * BBox() const {
        return this->bbox;
    }
    inline const glm::vec3 * CBox() const {
        return this->clipbox;
    }
    size_t size() const;
    const List& operator[](int idx) const;

    inline const std::string& get_filename(void) const {
        return this->filename;
    }
    inline unsigned int get_filetime(void) const {
        return this->filetime;
    }

    inline float get_timestamp(void) const {
        return this->timestamp;
    }

    inline void ForceBBox(const glm::vec3& minVal, const glm::vec3& maxVal) {
        this->bbox[0] = minVal;
        this->bbox[1] = maxVal;
    }
    inline void ForceCBox(const glm::vec3& minVal, const glm::vec3& maxVal) {
        this->clipbox[0] = minVal;
        this->clipbox[1] = maxVal;
    }

private:

    std::string filename;
    unsigned int filetime;

    glm::vec3 bbox[2];
    glm::vec3 clipbox[2];
    std::vector<uint8_t> data;
    std::vector<List> lists;
    float timestamp;
};

}
