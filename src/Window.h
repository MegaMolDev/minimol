#pragma once

#include <glm/glm.hpp>
#include <string>
#ifdef HAS_ANTTWEAKBAR
#include <AntTweakBar.h>
#endif
#include "Data.h"
#include <cstdint>
#include "CmdLineParser.h"
#include "ViewInteractor.h"

namespace minimol {

    class Renderer;

class Window {
public:
    typedef double scalar;

    Window(void);
    ~Window(void);

    void SetData(Data *dat);
    void SetRenderer(Renderer* rnd);

    void SetFromParser(CmdLineParser& parser);

private:
    static void glutDisplay();
    static void glutReshape(int w, int h);
    static void glutMouse(int btn, int state, int x, int y);
    static void glutMouseMotion(int x, int y);
    static void glutKeyboard(unsigned char key, int x, int y);
    static void glutKeyboardUp(unsigned char key, int x, int y);
    static void glutSpecialKeyboard(int keyCode, int x, int y);
    static void glutSpecialKeyboardUp(int keyCode, int x, int y);
    static void glutClose();

    void display();
    void resetCamera();
    void storeCamera();
    void restoreCamera();
#ifdef HAS_LIBPNG
    void saveScreenShot();
#endif
    void saveState();
    void loadState();
    void dumpMatrices();
    void calcClipPlanes(scalar& d_min, scalar& d_max);
    void updateProjMat();
    void saveData();

    void triggerDataGeneration();
    void setDataGeneratorView();
    void setScreenShotFileName();
    void advanceImageDataGenerator();

    int winID;
    unsigned int width, height;
#ifdef HAS_ANTTWEAKBAR
    TwBar *twBar;
#endif
    unsigned int winSetW, winSetH;

    Data *dat;
    Renderer *rnd;

    bool drawBBox;
    std::string camMem;

    std::string filename;
    uint32_t filetime;

    glm::vec3 lightDir;
    bool isCamLight;
    glm::mat4 projMat;
    float apAngRad;

    ViewInteractor cam;

#ifdef HAS_LIBPNG
    std::string screenShotPath;
    bool screenShotAlpha;
    bool makeDepthScreenShot;
    bool planScreenShot;
#endif
    std::string stateFilePath;
    std::string saveFilePath;

    float alpha_min, alpha_max, alpha_step;
    float beta_min, beta_max, beta_step;
    long long gen_state;
    float alpha_val, beta_val;
};

}
