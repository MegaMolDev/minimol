#include "Window.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Renderer.h"
#ifdef HAS_LIBPNG
#include "ImageWriter.h"
#endif
#include "StateFile.h"
#include <cctype>
#include "MMPLDWriter.h"

namespace minimol {

#ifdef HAS_ANTTWEAKBAR
void TW_CALL CopyStdStringToClient(std::string& destinationClientString, const std::string& sourceLibraryString) {
  // Copy the content of souceString handled by the AntTweakBar library to destinationClientString handled by your application
  destinationClientString = sourceLibraryString;
}

int TW_CALL myGlutGetModifiers(void) {
    return ::glutGetModifiers();
}
#endif

Window::Window(void) : dat(nullptr), rnd(nullptr), drawBBox(true), filename(), filetime(0),
        lightDir(-1.0, -1.0, 1.0), isCamLight(true), projMat(1.0f), apAngRad(glm::radians(30.0f)), cam(),
#ifdef HAS_LIBPNG
        screenShotPath("unnamed.png"), screenShotAlpha(false), makeDepthScreenShot(false), planScreenShot(false),
#endif
        stateFilePath("statefile.txt"), saveFilePath(),
        alpha_min(0.0f), alpha_max(90.0f), alpha_step(5.0f),
        beta_min(0.0f), beta_max(355.0f), beta_step(30.0f),
        gen_state(-1), alpha_val(0.0f), beta_val(0.0f) {

    // create window
    //::glutInitContextVersion(4, 5);
    //::glutInitContextProfile(/*GLUT_CORE_PROFILE /*/GLUT_COMPATIBILITY_PROFILE/**/);
    ::glutCreateWindow("MiniMol");
    this->winID = ::glutGetWindow();
    ::glutSetWindowData(this);
    ::glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    this->winSetW = this->width = std::max<unsigned int>(1, ::glutGet(GLUT_INIT_WINDOW_WIDTH));
    this->winSetH = this->height = std::max<unsigned int>(1, ::glutGet(GLUT_INIT_WINDOW_HEIGHT));

    ::glewExperimental = GL_TRUE;
    GLenum err = ::glewInit();
    if(err != GLEW_OK) {
        std::cerr << "glewInit failed" << std::endl;
        return;
    }
    if (!::glewIsSupported("GL_VERSION_3_3")) {
        std::cerr << "glewIsSupported(GL_VERSION_3_3) failed" << std::endl;
        return;
    }

    // setup glut callbacks
    ::glutDisplayFunc(&Window::glutDisplay);
    ::glutReshapeFunc(&Window::glutReshape);
    ::glutMouseFunc(&Window::glutMouse);
    ::glutMotionFunc(&Window::glutMouseMotion);
    ::glutPassiveMotionFunc(&Window::glutMouseMotion);
    ::glutKeyboardFunc(&Window::glutKeyboard);
    ::glutKeyboardUpFunc(&Window::glutKeyboardUp);
    ::glutSpecialFunc(&Window::glutSpecialKeyboard);
    ::glutSpecialUpFunc(&Window::glutSpecialKeyboardUp);
    ::glutCloseFunc(&Window::glutClose);

    // setup opengl
    ::glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

#ifdef HAS_ANTTWEAKBAR
    // setup atb
    ::TwInit(TW_OPENGL, NULL);
    ::TwGLUTModifiersFunc(&myGlutGetModifiers);
    ::TwCopyStdStringToClientFunc(&CopyStdStringToClient);
    ::TwDefine(" TW_HELP visible=false ");

    this->twBar = ::TwNewBar("MiniMol");
    ::TwDefine("MiniMol contained=true");
    std::string def("MiniMol position='10 10' size='200 ");
    def += StateFile::int_to_str(this->height - 20);
    def += "'";
    ::TwDefine(def.c_str());

    ::TwAddVarRW(this->twBar, "winW", TW_TYPE_UINT32, &this->winSetW, "label='Width' group='Window'");
    ::TwAddVarRW(this->twBar, "winH", TW_TYPE_UINT32, &this->winSetH, "label='Height' group='Window'");
    ::TwAddButton(this->twBar, "winGet", [](void*d) {
        static_cast<Window*>(d)->winSetW = static_cast<Window*>(d)->width;
        static_cast<Window*>(d)->winSetH = static_cast<Window*>(d)->height;
    }, this, "label='Get Size' group='Window'");
    ::TwAddButton(this->twBar, "winSet", [](void*d) {
        ::glutReshapeWindow(static_cast<Window*>(d)->winSetW, static_cast<Window*>(d)->winSetH);
    }, this, "label='Set Size' group='Window'");
    ::TwAddButton(this->twBar, "winClose", [](void*d) { ::glutDestroyWindow(static_cast<Window*>(d)->winID); }, this, "label='Close' group='Window' key=ESCAPE");
    //::TwDefine("MiniMol/Window opened=false");

    ::TwAddVarRO(this->twBar, "dataFileName", TW_TYPE_STDSTRING, &this->filename, "label='Filename' group='data'");
    ::TwAddVarRO(this->twBar, "dataFileTime", TW_TYPE_UINT32, &this->filetime, "label='Time' group='data'");
    ::TwDefine("MiniMol/data label='Data Set'");

    ::TwAddVarRW(this->twBar, "saveFilePath", TW_TYPE_STDSTRING, &this->saveFilePath, "label='Save Filename' group='savedata'");
    ::TwAddButton(this->twBar, "saveFile", [](void*d) { static_cast<Window*>(d)->saveData(); }, this, "label='Save' group='savedata'");
    ::TwDefine("MiniMol/savedata label='Save' group='data'");

    ::TwAddVarRW(this->twBar, "drawBBox", TW_TYPE_BOOLCPP, &this->drawBBox, "label='Show Bounding Box'");

    ::TwAddButton(this->twBar, "camReset", [](void*d) { static_cast<Window*>(d)->resetCamera(); }, this, "label='Reset' group='Camera' key=HOME");
    ::TwAddVarRW(this->twBar, "camMem", TW_TYPE_STDSTRING, &this->camMem, "label='Memory' group='Camera'");
    ::TwAddButton(this->twBar, "camStore", [](void*d) { static_cast<Window*>(d)->storeCamera(); }, this, "label='Store' group='Camera'");
    ::TwAddButton(this->twBar, "camRestore", [](void*d) { static_cast<Window*>(d)->restoreCamera(); }, this, "label='Restore' group='Camera'");

    ::TwAddVarRW(this->twBar, "lightDir", TW_TYPE_DIR3F, glm::value_ptr(this->lightDir), "label='Direction' group='Light' axisx=-x axisy=z axisz=-y");
    ::TwAddVarRW(this->twBar, "lightIsCamLight", TW_TYPE_BOOLCPP, &this->isCamLight, "label='Fixed at Camera' group='Light'");
    ::TwDefine("MiniMol/Light label='Light'");

#ifdef HAS_LIBPNG
    ::TwAddVarRW(this->twBar, "picPath", TW_TYPE_STDSTRING, &this->screenShotPath, "label='Image Path' group='ScreenShot' ");
    ::TwAddVarRW(this->twBar, "picAlpha", TW_TYPE_BOOLCPP, &this->screenShotAlpha, "label='Store Alpha' group='ScreenShot'");
    ::TwAddVarRW(this->twBar, "picDepth", TW_TYPE_BOOLCPP, &this->makeDepthScreenShot, "label='Store Depth Image' group='ScreenShot'");
    ::TwAddButton(this->twBar, "picMake", [](void*d) { static_cast<Window*>(d)->planScreenShot = true; }, this, "label='Create and Save' group='ScreenShot'");
    ::TwDefine("MiniMol/ScreenShot label='Screen Shot'");
#endif

    ::TwAddVarRW(this->twBar, "statePath", TW_TYPE_STDSTRING, &this->stateFilePath, "label='Path' group='State'");
    ::TwAddButton(this->twBar, "stateSave", [](void*d) { static_cast<Window*>(d)->saveState(); }, this, "label='Save' group='State'");
    ::TwAddButton(this->twBar, "stateLoad", [](void*d) { static_cast<Window*>(d)->loadState(); }, this, "label='Load' group='State'");
    ::TwDefine("MiniMol/State label='State File'");

    ::TwAddButton(this->twBar, "dumpMatrices", [](void*d) { static_cast<Window*>(d)->dumpMatrices(); }, this, "label='Dump Matrices'");

#ifdef HAS_LIBPNG
    ::TwAddVarRW(this->twBar, "alphaMin", TW_TYPE_FLOAT, &this->alpha_min, "label='Horiz. Angle Start' min=0 max=360 group='ImgDataGen' step=5 precision=2 ");
    ::TwAddVarRW(this->twBar, "alphaMax", TW_TYPE_FLOAT, &this->alpha_max, "label='Horiz. Angle End' min=0 max=360 group='ImgDataGen' step=5 precision=2 ");
    ::TwAddVarRW(this->twBar, "alphaStep", TW_TYPE_FLOAT, &this->alpha_step, "label='Horiz. Angle Step' min=0.01 max=360 group='ImgDataGen' step=5 precision=2 ");
    ::TwAddVarRW(this->twBar, "betaMin", TW_TYPE_FLOAT, &this->beta_min, "label='Roll Angle Start' min=0 max=360 group='ImgDataGen' step=5 precision=2 ");
    ::TwAddVarRW(this->twBar, "betaMax", TW_TYPE_FLOAT, &this->beta_max, "label='Roll Angle End' min=0 max=360 group='ImgDataGen' step=5 precision=2 ");
    ::TwAddVarRW(this->twBar, "betaStep", TW_TYPE_FLOAT, &this->beta_step, "label='Roll Angle Step' min=0.01 max=360 group='ImgDataGen' step=5 precision=2 ");
    ::TwAddButton(this->twBar, "imgDataGenTrigger", [](void*d) { static_cast<Window*>(d)->triggerDataGeneration(); }, this, "label='Generate' group='ImgDataGen' ");
    ::TwDefine("MiniMol/ImgDataGen label='Image Data Generator'");
#endif

#endif

    // finalize
    ::glutShowWindow();
    ::glutReshapeWindow(this->winSetW, this->winSetH);
#ifdef HAS_ANTTWEAKBAR
    ::TwWindowSize(this->width, this->height);
#endif

    this->resetCamera();
}

Window::~Window(void) {
    this->SetRenderer(nullptr);
#ifdef HAS_ANTTWEAKBAR
    ::TwTerminate();
#endif
    if (this->winID != 0) {
        this->winID = 0;
        ::glutSetWindowData(nullptr);
        ::glutHideWindow();
        ::glutDestroyWindow(::glutGetWindow());
    }
}

void Window::SetData(Data *dat) {
    this->dat = dat;
    if (dat != nullptr) {
        this->filename = dat->get_filename();
        this->filetime = dat->get_filetime();
    } else {
        this->filename.clear();
        this->filetime = 0;
    }
    this->resetCamera();
}

void Window::SetRenderer(Renderer* rnd) {
    if (this->rnd != rnd) {
        if (this->rnd != nullptr) {
            this->rnd->deinit();
        }
        this->rnd = rnd;
        if (this->rnd != nullptr) {
            this->rnd->init();
            this->rnd->SetCamParam(&cam);
        }
    }
}

void Window::SetFromParser(CmdLineParser& parser) {
    if (parser.WindowSize(this->winSetW, this->winSetH)) {
        ::glutReshapeWindow(this->winSetW, this->winSetH);
    }

    glm::vec3 cp, cla, cu;
    float caa;
    if (parser.Camera(cp, cla, cu, caa)) {
        cam.eye = cp;
        cam.focus = cla;
        cam.up_dir = cu;
        this->apAngRad = glm::radians(caa);
        this->updateProjMat();
    }

    parser.ShowBBox(this->drawBBox);
    parser.Light(this->lightDir);
    parser.CamLight(this->isCamLight);
}

#if /* REGION: GLUT callbacks */ 1
void Window::glutDisplay() {
    ::glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that != nullptr) {
        that->display();
    }

#ifdef HAS_ANTTWEAKBAR
    if (that->gen_state < 0) ::TwDraw();
#endif

    ::glutSwapBuffers();
    ::glutPostRedisplay();
}

void Window::glutReshape(int w, int h) {
    ::glViewport(0, 0, w, h);
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that != nullptr) {
        that->width = std::max<unsigned int>(1, w);
        that->height = std::max<unsigned int>(1, h);
#ifdef HAS_ANTTWEAKBAR
        ::TwWindowSize(that->width, that->height);
#endif
        that->cam.setViewportSize(w, h);
    }
}

void Window::glutMouse(int btn, int state, int x, int y) {
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that->gen_state >= 0) return;
#ifdef HAS_ANTTWEAKBAR
    if (::TwEventMouseButtonGLUT(btn, state, x, y)) {
        if (state == GLUT_DOWN) return;
    }
#endif
    if (that != nullptr) {
        int modifiers = glutGetModifiers();
        that->cam.updateModifiers(
            (modifiers & GLUT_ACTIVE_SHIFT) == GLUT_ACTIVE_SHIFT,
            (modifiers & GLUT_ACTIVE_ALT) == GLUT_ACTIVE_ALT,
            (modifiers & GLUT_ACTIVE_CTRL) == GLUT_ACTIVE_CTRL);
        switch (btn) {
        case GLUT_LEFT_BUTTON: that->cam.updateLeftButton(state == GLUT_DOWN); break;
        case GLUT_MIDDLE_BUTTON: that->cam.updateMiddleButton(state == GLUT_DOWN); break;
        case GLUT_RIGHT_BUTTON: that->cam.updateRightButton(state == GLUT_DOWN); break;
        }
        that->cam.updatePosition(x, y);
    }
}

void Window::glutMouseMotion(int x, int y) {
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that->gen_state >= 0) return;
#ifdef HAS_ANTTWEAKBAR
    if (::TwEventMouseMotionGLUT(x, y)) return;
#endif
    if (that != nullptr) {
        that->cam.updatePosition(x, y);
    }
}

void Window::glutKeyboard(unsigned char key, int x, int y) {
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that->gen_state >= 0) return;
#ifdef HAS_ANTTWEAKBAR
    if (::TwEventKeyboardGLUT(key, x, y)) return;
#endif
    if (that != nullptr) {
        int modifiers = glutGetModifiers();
        that->cam.updateModifiers(
            (modifiers & GLUT_ACTIVE_SHIFT) == GLUT_ACTIVE_SHIFT,
            (modifiers & GLUT_ACTIVE_ALT) == GLUT_ACTIVE_ALT,
            (modifiers & GLUT_ACTIVE_CTRL) == GLUT_ACTIVE_CTRL);
        that->cam.updateKeys(key == ' ');
        that->cam.updatePosition(x, y);
    }
}

void Window::glutKeyboardUp(unsigned char key, int x, int y) {
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that->gen_state >= 0) return;
    if (that != nullptr) {
        int modifiers = glutGetModifiers();
        that->cam.updateModifiers(
            (modifiers & GLUT_ACTIVE_SHIFT) == GLUT_ACTIVE_SHIFT,
            (modifiers & GLUT_ACTIVE_ALT) == GLUT_ACTIVE_ALT,
            (modifiers & GLUT_ACTIVE_CTRL) == GLUT_ACTIVE_CTRL);
        that->cam.updateKeys(false);
        that->cam.updatePosition(x, y);
    }
}

void Window::glutSpecialKeyboard(int keyCode, int x, int y) {
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that->gen_state >= 0) return;
#ifdef HAS_ANTTWEAKBAR
    if (::TwEventSpecialGLUT(keyCode, x, y)) return;
#endif
    if (that != nullptr) {
        that->cam.updatePosition(x, y);
    }
}

void Window::glutSpecialKeyboardUp(int keyCode, int x, int y) {
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that->gen_state >= 0) return;
    if (that != nullptr) {
        that->cam.updatePosition(x, y);
    }
}

void Window::glutClose() {
    Window* that = static_cast<Window*>(::glutGetWindowData());
    if (that != nullptr) {
        that->SetRenderer(nullptr);
        that->winID = 0;
        ::glutSetWindowData(nullptr);
    }
}
#endif

void Window::display() {
    this->updateProjMat();
    ::glMatrixMode(GL_PROJECTION);
    ::glLoadMatrixf(glm::value_ptr(projMat));
    ::glMatrixMode(GL_MODELVIEW);
    ::glLoadMatrixd(glm::value_ptr(glm::lookAt(cam.eye, cam.focus, cam.up_dir)));

    ::glEnable(GL_DEPTH_TEST);
    ::glEnable(GL_LIGHTING);

    float lightPos[] = { this->lightDir.x, this->lightDir.y, this->lightDir.z, 0.0 };
    if (this->isCamLight) {
        glm::vec3 lp
            = glm::vec3(this->cam.right_dir()) * lightPos[0]
            + glm::vec3(this->cam.front_dir()) * lightPos[1]
            + glm::vec3(this->cam.up_dir) * lightPos[2];
        lightPos[0] = lp.x;
        lightPos[1] = lp.y;
        lightPos[2] = lp.z;
    }
    ::glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    if (this->rnd) {
        this->rnd->draw();
    }

    ::glDisable(GL_LIGHTING);

    // Draw bbox
    if (drawBBox && (this->dat != nullptr)) {
        const glm::vec3* bbox = this->dat->BBox();
        float x1 = bbox[0].x;
        float x2 = bbox[1].x;
        float y1 = bbox[0].y;
        float y2 = bbox[1].y;
        float z1 = bbox[0].z;
        float z2 = bbox[1].z;
        ::glColor3ub(192,192,192);
        ::glBegin(GL_LINES);
            ::glVertex3f(x1, y1, z1); ::glVertex3f(x1, y1, z2);
            ::glVertex3f(x2, y1, z1); ::glVertex3f(x2, y1, z2);
            ::glVertex3f(x1, y2, z1); ::glVertex3f(x1, y2, z2);
            ::glVertex3f(x2, y2, z1); ::glVertex3f(x2, y2, z2);

            ::glVertex3f(x1, y1, z1); ::glVertex3f(x1, y2, z1);
            ::glVertex3f(x2, y1, z1); ::glVertex3f(x2, y2, z1);
            ::glVertex3f(x1, y1, z2); ::glVertex3f(x1, y2, z2);
            ::glVertex3f(x2, y1, z2); ::glVertex3f(x2, y2, z2);

            ::glVertex3f(x1, y1, z1); ::glVertex3f(x2, y1, z1);
            ::glVertex3f(x1, y2, z1); ::glVertex3f(x2, y2, z1);
            ::glVertex3f(x1, y1, z2); ::glVertex3f(x2, y1, z2);
            ::glVertex3f(x1, y2, z2); ::glVertex3f(x2, y2, z2);
        ::glEnd();
    }

#ifdef HAS_LIBPNG
    if (this->planScreenShot) {
        this->planScreenShot = false;
        ::glFlush();
        this->saveScreenShot();
    }
#endif

    if (gen_state >= 0) {
        advanceImageDataGenerator();
        // Update camera
        setDataGeneratorView();
#ifdef HAS_LIBPNG
        // Set screenshot file name
        if (gen_state >= 0) this->planScreenShot = true;
        setScreenShotFileName();
#endif
    }

}

void Window::resetCamera() {
    if (this->dat == nullptr) return;

    this->apAngRad = glm::radians(30.0f);

    glm::vec3 mip = (this->dat->BBox()[0] + this->dat->BBox()[1]) * 0.5f;
    float maxd =
        std::max<float>(
            std::abs(dat->CBox()[1].x - dat->CBox()[0].x),
        std::max<float>(
            std::abs(dat->CBox()[1].y - dat->CBox()[0].y),
            std::abs(dat->CBox()[1].z - dat->CBox()[0].z)
        ) );

    this->cam.eye = mip + glm::vec3(0.0f, 1.25f * (-(0.5f * maxd) / std::tan(0.5f * this->apAngRad)), 0.0f);
    this->cam.focus = mip;
    this->cam.up_dir = glm::vec3(0.0f, 0.0f, 1.0f);
    this->cam.zoom_speed = maxd * 0.01f;

    this->updateProjMat();
}

void Window::storeCamera() {
    std::stringstream ser;
    ser << this->cam.eye.x << ";";
    ser << this->cam.eye.y<< ";";
    ser << this->cam.eye.z << ";";
    ser << this->cam.focus.x << ";";
    ser << this->cam.focus.y << ";";
    ser << this->cam.focus.z << ";";
    ser << this->cam.up_dir.x << ";";
    ser << this->cam.up_dir.y << ";";
    ser << this->cam.up_dir.z << ";";
    ser << glm::degrees(this->apAngRad);
    this->camMem = ser.str();
}

void Window::restoreCamera() {
    try {
        std::stringstream ser(this->camMem);
        char sepc;
        float x[3], y[3], z[3], a;
        ser >> x[0] >> sepc >> y[0] >> sepc >> z[0] >> sepc;
        ser >> x[1] >> sepc >> y[1] >> sepc >> z[1] >> sepc;
        ser >> x[2] >> sepc >> y[2] >> sepc >> z[2] >> sepc;
        ser >> a;

        this->cam.eye.x = x[0];
        this->cam.eye.y = y[0];
        this->cam.eye.z = z[0];
        this->cam.focus.x = x[1];
        this->cam.focus.y = y[1];
        this->cam.focus.z = z[1];
        this->cam.up_dir.x = x[2];
        this->cam.up_dir.y = y[2];
        this->cam.up_dir.z = z[2];

        this->apAngRad = glm::radians(a);
        this->updateProjMat();

    } catch (const std::exception& ex) {
        std::cerr << "Failed to restore camera parameter: " << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "Failed to restore camera parameter: unknown exception" << std::endl;
    }
}

#ifdef HAS_LIBPNG
void Window::saveScreenShot() {
    ::glPixelStorei(GL_PACK_ALIGNMENT, 1);

    std::string fn(this->screenShotPath);
    std::string fnext;

    size_t fn_len = fn.length();
    if (fn_len > 4) {
        fnext = fn.substr(fn_len - 4, 4);
        for (int i = 0; i < 4; i++) fnext[i] = std::tolower(fnext[i]);
        if (fnext.compare(".png") == 0) {
            fn = fn.substr(0, fn_len - 4);
            fn_len -= 4;
        } else {
            fnext = ".png";
        }
    } else {
        fnext = ".png";
    }
    if (fn_len <= 0) {
        fn = "unnamed";
    }

    ImageWriter img;

    unsigned char *imgdat = new unsigned char[this->width * this->height * (this->screenShotAlpha ? 4 : 3)];
    ::glReadPixels(0, 0, this->width, this->height, this->screenShotAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, imgdat);

    img.save((fn + fnext).c_str(), this->width, this->height,
        this->screenShotAlpha ? ImageWriter::DataType::RGBAu8 : ImageWriter::DataType::RGBu8, imgdat);

    delete[] imgdat;

    if (this->makeDepthScreenShot) {
        float *depthdat = new float[this->width * this->height];
        ::glReadPixels(0, 0, this->width, this->height, GL_DEPTH_COMPONENT, GL_FLOAT, depthdat);

        img.save((fn + "_depth" + fnext).c_str(), this->width, this->height,
            ImageWriter::DataType::Df, depthdat);

        delete[] depthdat;
    }
}
#endif

void Window::saveState() {
    StateFile state;

    state["datafile"] = this->filename;
    state["datatime"] = StateFile::int_to_str(this->filetime);
    state["width"] = StateFile::int_to_str(static_cast<int>(this->width));
    state["height"] = StateFile::int_to_str(static_cast<int>(this->height));
    state["campos.x"] = StateFile::double_to_str(this->cam.eye.x);
    state["campos.y"] = StateFile::double_to_str(this->cam.eye.y);
    state["campos.z"] = StateFile::double_to_str(this->cam.eye.z);
    state["camlookat.x"] = StateFile::double_to_str(this->cam.focus.x);
    state["camlookat.y"] = StateFile::double_to_str(this->cam.focus.y);
    state["camlookat.z"] = StateFile::double_to_str(this->cam.focus.z);
    state["camup.x"] = StateFile::double_to_str(this->cam.up_dir.x);
    state["camup.y"] = StateFile::double_to_str(this->cam.up_dir.y);
    state["camup.z"] = StateFile::double_to_str(this->cam.up_dir.z);
    state["cam.apang"] = StateFile::double_to_str(glm::degrees(this->apAngRad));
    scalar d_min, d_max;
    calcClipPlanes(d_min, d_max);
    state["cam.near"] = StateFile::double_to_str(d_min);
    state["cam.far"] = StateFile::double_to_str(d_max);
    if (this->dat != nullptr) {
        state["bbox.x1"] = StateFile::double_to_str(std::min<float>(this->dat->BBox()[0].x, this->dat->BBox()[1].x));
        state["bbox.y1"] = StateFile::double_to_str(std::min<float>(this->dat->BBox()[0].y, this->dat->BBox()[1].y));
        state["bbox.z1"] = StateFile::double_to_str(std::min<float>(this->dat->BBox()[0].z, this->dat->BBox()[1].z));
        state["bbox.x2"] = StateFile::double_to_str(std::max<float>(this->dat->BBox()[1].x, this->dat->BBox()[0].x));
        state["bbox.y2"] = StateFile::double_to_str(std::max<float>(this->dat->BBox()[1].y, this->dat->BBox()[0].y));
        state["bbox.z2"] = StateFile::double_to_str(std::max<float>(this->dat->BBox()[1].z, this->dat->BBox()[0].z));
        state["cbox.x1"] = StateFile::double_to_str(std::min<float>(this->dat->CBox()[0].x, this->dat->CBox()[1].x));
        state["cbox.y1"] = StateFile::double_to_str(std::min<float>(this->dat->CBox()[0].y, this->dat->CBox()[1].y));
        state["cbox.z1"] = StateFile::double_to_str(std::min<float>(this->dat->CBox()[0].z, this->dat->CBox()[1].z));
        state["cbox.x2"] = StateFile::double_to_str(std::max<float>(this->dat->CBox()[1].x, this->dat->CBox()[0].x));
        state["cbox.y2"] = StateFile::double_to_str(std::max<float>(this->dat->CBox()[1].y, this->dat->CBox()[0].y));
        state["cbox.z2"] = StateFile::double_to_str(std::max<float>(this->dat->CBox()[1].z, this->dat->CBox()[0].z));
    }
    state["showbbox"] = StateFile::int_to_str(this->drawBBox ? 1 : 0);
    state["light.x"] = StateFile::double_to_str(this->lightDir.x);
    state["light.y"] = StateFile::double_to_str(this->lightDir.y);
    state["light.z"] = StateFile::double_to_str(this->lightDir.z);
    state["lightatcam"] = StateFile::int_to_str(this->isCamLight ? 1 : 0);

    try {
        state.save(this->stateFilePath);

        std::cout << "State file \"" << this->stateFilePath << "\" saved." << std::endl;
    } catch(std::exception ex) {
        std::cerr << "Failed to save state file \"" << this->stateFilePath << "\": " << ex.what() << std::endl;
    } catch(...) {
        std::cerr << "Failed to save state file \"" << this->stateFilePath << "\": unexpected exception" << std::endl;
    }
}

void Window::loadState() {
    StateFile state;
    try {
        state.load(this->stateFilePath);
        std::cout << "State file \"" << this->stateFilePath << "\" loaded." << std::endl;

        int datCnt = 0;
        state.try_assign("datafile", [&](const std::string& str) { this->filename = str; datCnt++; });
        state.try_assign_int("datatime", [&](int val) { this->filetime = val; datCnt++; });
        if ((datCnt > 0) && (this->dat != nullptr)) {
            if ((this->filename.compare(this->dat->get_filename()) != 0)
                    || (this->dat->get_filetime() != this->filetime)) {
                this->dat->loadMMPLD(this->filename.c_str(), this->filetime);
                this->filename = this->dat->get_filename();
                this->filetime = this->dat->get_filetime();
                this->resetCamera();
            }
        }

        int setWidth, setHeight;
        int sizeCnt = 0;
        state.try_assign_int("width", [&](int val) { setWidth = val; sizeCnt++; });
        state.try_assign_int("height", [&](int val) { setHeight = val; sizeCnt++; });
        if (sizeCnt == 2) {
            this->winSetW = setWidth;
            this->winSetH = setHeight;
            ::glutReshapeWindow(this->winSetW, this->winSetH);
        }

        float camDat[10];
        int camCnt = 0;
        state.try_assign_double("campos.x", [&](double val) { camDat[0] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("campos.y", [&](double val) { camDat[1] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("campos.z", [&](double val) { camDat[2] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("camlookat.x", [&](double val) { camDat[3] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("camlookat.y", [&](double val) { camDat[4] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("camlookat.z", [&](double val) { camDat[5] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("camup.x", [&](double val) { camDat[6] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("camup.y", [&](double val) { camDat[7] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("camup.z", [&](double val) { camDat[8] = static_cast<float>(val); camCnt++; });
        state.try_assign_double("cam.apang", [&](double val) { camDat[9] = static_cast<float>(val); camCnt++; });
        if (camCnt == 10) {
            this->cam.eye = glm::vec3(camDat[0], camDat[1], camDat[2]);
            this->cam.focus = glm::vec3(camDat[3], camDat[4], camDat[5]);
            this->cam.up_dir = glm::vec3(camDat[6], camDat[7], camDat[8]);
            this->apAngRad = glm::radians(camDat[9]);
        }

        // clipping distances will be computed
        //state.try_assign_double("cam.near", [&](double val) { camDat[10] = static_cast<float>(val); camCnt++; });
        //state.try_assign_double("cam.far", [&](double val) { camDat[11] = static_cast<float>(val); camCnt++; });
        this->updateProjMat();

        // bounding box data is usable, since the data set is specifying these anew
        // state["bbox.x1"] = StateFile::double_to_str(std::min<float>(this->dat->BBox().Left(), this->dat->BBox().Right()));
        // state["bbox.y1"] = StateFile::double_to_str(std::min<float>(this->dat->BBox().Bottom(), this->dat->BBox().Top()));
        // state["bbox.z1"] = StateFile::double_to_str(std::min<float>(this->dat->BBox().Back(), this->dat->BBox().Front()));
        // state["bbox.x2"] = StateFile::double_to_str(std::max<float>(this->dat->BBox().Right(), this->dat->BBox().Left()));
        // state["bbox.y2"] = StateFile::double_to_str(std::max<float>(this->dat->BBox().Top(), this->dat->BBox().Bottom()));
        // state["bbox.z2"] = StateFile::double_to_str(std::max<float>(this->dat->BBox().Front(), this->dat->BBox().Back()));
        // state["cbox.x1"] = StateFile::double_to_str(std::min<float>(this->dat->CBox().Left(), this->dat->CBox().Right()));
        // state["cbox.y1"] = StateFile::double_to_str(std::min<float>(this->dat->CBox().Bottom(), this->dat->CBox().Top()));
        // state["cbox.z1"] = StateFile::double_to_str(std::min<float>(this->dat->CBox().Back(), this->dat->CBox().Front()));
        // state["cbox.x2"] = StateFile::double_to_str(std::max<float>(this->dat->CBox().Right(), this->dat->CBox().Left()));
        // state["cbox.y2"] = StateFile::double_to_str(std::max<float>(this->dat->CBox().Top(), this->dat->CBox().Bottom()));
        // state["cbox.z2"] = StateFile::double_to_str(std::max<float>(this->dat->CBox().Front(), this->dat->CBox().Back()));

        state.try_assign_int("showbbox", [&](int val) { this->drawBBox = (val != 0); });

        float light[3];
        int lightCnt = 0;
        state.try_assign_double("light.x", [&](double val) { light[0] = static_cast<float>(val); lightCnt++; });
        state.try_assign_double("light.y", [&](double val) { light[1] = static_cast<float>(val); lightCnt++; });
        state.try_assign_double("light.z", [&](double val) { light[2] = static_cast<float>(val); lightCnt++; });
        if (lightCnt == 3) {
            this->lightDir = glm::vec3(light[0], light[1], light[2]);
        }

        state.try_assign_int("lightatcam", [&](int val) { this->isCamLight = (val != 0); });

    } catch(std::exception ex) {
        std::cerr << "Failed to load state file \"" << this->stateFilePath << "\": " << ex.what() << std::endl;
    } catch(...) {
        std::cerr << "Failed to load state file \"" << this->stateFilePath << "\": unexpected exception" << std::endl;
    }
}

void Window::dumpMatrices(void) {
    std::cout << "Projection Matrix:" << glm::to_string(this->projMat) << std::endl;
    std::cout << "View Matrix:" << glm::to_string(glm::lookAt(cam.eye, cam.focus, cam.up_dir)) << std::endl;
    float lightPos[] = { this->lightDir.x, this->lightDir.y, this->lightDir.z, 0.0f };
    if (this->isCamLight) {
        glm::vec3 lp
            = glm::vec3(this->cam.right_dir()) * lightPos[0]
            + glm::vec3(this->cam.front_dir()) * lightPos[1]
            + glm::vec3(this->cam.up_dir) * lightPos[2];
        lightPos[0] = lp.x;
        lightPos[1] = lp.y;
        lightPos[2] = lp.z;
    }
    std::cout << "Light: " << lightPos[0] << ", " << lightPos[1] << ", " << lightPos[2] << ", " << lightPos[3] << std::endl;
}

void Window::calcClipPlanes(scalar& d_min, scalar& d_max) {
    glm::vec3 pos = cam.eye;
    glm::vec3 lookAt = cam.focus;

    glm::vec3 front = glm::normalize(lookAt - pos);
    glm::vec3 const *box = dat->CBox();
    const float box_edge =
        std::max<float>(std::abs(box[1].x - box[0].x),
        std::max<float>(std::abs(box[1].y - box[0].y),
        std::abs(box[1].z - box[0].z)
        ));

    float d0 = glm::dot(front, glm::vec3(box[0].x, box[0].y, box[0].z) - pos);
    float d1 = glm::dot(front, glm::vec3(box[1].x, box[0].y, box[0].z) - pos);
    float d2 = glm::dot(front, glm::vec3(box[0].x, box[1].y, box[0].z) - pos);
    float d3 = glm::dot(front, glm::vec3(box[1].x, box[1].y, box[0].z) - pos);
    float d4 = glm::dot(front, glm::vec3(box[0].x, box[0].y, box[1].z) - pos);
    float d5 = glm::dot(front, glm::vec3(box[1].x, box[0].y, box[1].z) - pos);
    float d6 = glm::dot(front, glm::vec3(box[0].x, box[1].y, box[1].z) - pos);
    float d7 = glm::dot(front, glm::vec3(box[1].x, box[1].y, box[1].z) - pos);

    d_min =
        std::min<float>(
        std::min<float>(
        std::min<float>(d0, d1),
        std::min<float>(d2, d3)),
        std::min<float>(
        std::min<float>(d4, d5),
        std::min<float>(d6, d7)));
    d_max =
        std::max<float>(
        std::max<float>(
        std::max<float>(d0, d1),
        std::max<float>(d2, d3)),
        std::max<float>(
        std::max<float>(d4, d5),
        std::max<float>(d6, d7)));

    const float border = 0.01f * box_edge;

    d_min -= border;
    d_max += border;

    const float dist_quot = 0.001f;
    if (d_min < d_max * dist_quot) d_min = d_max * dist_quot;
}

void Window::updateProjMat() {
    scalar d_min, d_max;
    calcClipPlanes(d_min, d_max);
    projMat = glm::perspective(this->apAngRad, static_cast<float>(this->width) / static_cast<float>(std::max<unsigned int>(this->height, 1)), static_cast<float>(d_min), static_cast<float>(d_max));
}

void Window::saveData() {
    if (saveFilePath.empty()) {
        std::cerr << "You must specify a file path to save to" << std::endl;
        return;
    }

    MMPLDWriter writer;
    try {

        if (!writer.CreateOpenFile(saveFilePath, 1)) throw std::runtime_error("MMPLDWriter::CreateFile failed");
        if (!writer.WriteDataFrame(*dat)) throw std::runtime_error("MMPLDWriter::WriteDataFrame failed");
        if (!writer.CloseFile()) throw std::runtime_error("MMPLDWriter::CloseFile failed");

    } catch (const std::exception& e) {
        std::cerr << "Failed to write file: " << e.what() << std::endl;
    } catch (...) {
        std::cerr << "Failed to write file: unexpected exception" << std::endl;
    }

}

void Window::triggerDataGeneration() {
#ifdef HAS_LIBPNG
    if (this->dat == nullptr) return;

    // Write Meta-Data file
    std::string fn = screenShotPath;
    size_t pos = fn.find_last_of('.');
    if (pos != std::string::npos) {
        fn.resize(pos);
    }

    std::ofstream meta;
    meta.open(fn + "_info.txt", std::ios::out);
    if (meta.is_open()) {

        alpha_val = alpha_min;
        beta_val = beta_min;
        gen_state = 0; // file #0

        setDataGeneratorView();

        meta << "data " << filename << std::endl
            << "timestep " << filetime << std::endl;
        meta << "boundingbox "
            << std::min<scalar>(this->dat->CBox()[0].x, this->dat->CBox()[1].x) << ", "
            << std::min<scalar>(this->dat->CBox()[0].y, this->dat->CBox()[1].y) << ", "
            << std::min<scalar>(this->dat->CBox()[0].z, this->dat->CBox()[1].z) << ", "
            << std::max<scalar>(this->dat->CBox()[1].x, this->dat->CBox()[0].x) << ", "
            << std::max<scalar>(this->dat->CBox()[1].y, this->dat->CBox()[0].y) << ", "
            << std::max<scalar>(this->dat->CBox()[1].z, this->dat->CBox()[0].z) << std::endl;
        meta << "aperture " << glm::degrees(apAngRad) << std::endl;
        meta << std::endl;

        glm::dvec3 baseDir(0.0, 1.0, 0.0);

        while (gen_state >= 0) {
            setDataGeneratorView();
            setScreenShotFileName();
            meta << "image " << gen_state << std::endl
                << "file " << screenShotPath << std::endl;

            scalar d_min, d_max;
            calcClipPlanes(d_min, d_max);

            meta << "camera (" << cam.focus.x << ", " << cam.focus.y << ", " << cam.focus.z << "), ("
                << cam.eye.x << ", " << cam.eye.y << ", " << cam.eye.z << "), ("
                << cam.up_dir.x << ", " << cam.up_dir.y << ", " << cam.up_dir.z << ")" << std::endl;
            meta << "clipdists " << d_min << ", " << d_max << std::endl;

            glm::dvec3 dir = glm::normalize(glm::dvec3(cam.focus) - glm::dvec3(cam.eye));
            scalar ang = glm::degrees(std::acos(glm::dot(dir, baseDir)));
            meta << "offangle " << ang << std::endl;
            meta << std::endl;

            advanceImageDataGenerator();
        }

        meta.flush();
        meta.close();
    }

    alpha_val = alpha_min;
    beta_val = beta_min;

    // Update camera
    setDataGeneratorView();
    gen_state = 0; // file #0

    // Set screenshot file name
    this->planScreenShot = true;
    setScreenShotFileName();
#endif
}

void Window::setDataGeneratorView() {

    this->apAngRad = glm::radians(30.0f);

    glm::dvec3 mip = (this->dat->BBox()[0] + this->dat->BBox()[1]) * 0.5f;
    scalar maxd =
        std::max<scalar>(
            std::abs(dat->CBox()[1].x - dat->CBox()[0].x),
            std::max<scalar>(
                std::abs(dat->CBox()[1].y - dat->CBox()[0].y),
                std::abs(dat->CBox()[1].z - dat->CBox()[0].z)
                ));

    this->cam.focus = mip;

    scalar dist = 1.25 * (-(0.5 * maxd) / std::tan(0.5 * this->apAngRad));

    glm::vec3 rotAx(std::sin(glm::radians(beta_val)), 0.0f, std::cos(glm::radians(beta_val)));
    glm::mat4 rotMat = glm::rotate(glm::mat4(), glm::radians(alpha_val), rotAx);

    this->cam.eye = mip + glm::dvec3(rotMat * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f)) * dist;
    this->cam.up_dir = glm::dvec3(rotMat * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f));

    this->cam.zoom_speed = maxd * 0.01f;

    this->updateProjMat();
}

void Window::setScreenShotFileName() {
#ifdef HAS_LIBPNG
    std::string fn = screenShotPath;
    std::string ext = "";

    size_t pos = fn.find_last_of('.');
    if (pos != std::string::npos) {
        ext = fn.substr(pos);
        fn.resize(pos);
    }

    pos = fn.find_last_not_of("0123456789");
    if (pos != std::string::npos) {
        if (fn[pos] != '_') pos++;
        fn.resize(pos);
    } else {
        fn = "unnamed";
    }

    std::stringstream nfn;
    nfn << fn;
    if (gen_state >= 0) nfn << '_' << std::setw(5) << std::setfill('0') << gen_state;
    nfn << ext;

    screenShotPath = nfn.str();

#endif
}

void Window::advanceImageDataGenerator() {
    gen_state++;
    alpha_val += alpha_step;
    if (alpha_val > alpha_max) {
        alpha_val = alpha_min;
        if (alpha_min <= 0.0f) alpha_val += alpha_step;
        beta_val += beta_step;
        if (beta_val > beta_max) {
            gen_state = -1;
        }
    }
}

}
